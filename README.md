# docker_laravel

For PHP 8.0.2
Configuración inicial para una aplicaion laravel en docker

-MYSQL
-NGINX
-PHP
-PHPMYADMIN

Se modifico el archivo .yml para que acepte varibles del entorno
Se modifico el archivo conf de nginx para que acepte variables del entorno

Para detener los containers
docker rm -f $(docker ps -a -q)
sudo service docker restart